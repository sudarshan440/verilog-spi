// Code your design here
module top(clk,rst);
  
  input clk;
  input rst;
  
  wire validation =1'b0;
  wire sck;
  wire mosi;
  wire miso;
  wire ss;
  
  //spi_master(clk,rst,sck,mosi,miso,start,ssel);
  spi_master master(clk,rst,sck,mosi,miso,ss);
  //spi_slave(clk,rst,sck,mosi,miso,ssel,slave_received_data)
  spi_slave slave (clk,rst,sck,mosi,miso,ss);

  
 // assign validation = (master_data == slave_received_data)?1:0;
  
endmodule 