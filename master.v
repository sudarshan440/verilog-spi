module spi_master(clk,rst,sck,mosi,miso,ss);

input clk,rst;
input miso;
output  mosi;
output  sck;
output reg ss;
//SPCR= 8'b01110001; neg_edge, lsb 
//SPCR= 8'b01111101; neg_edge, lsb 
  
//SPCR= 8'b01010001; neg_edge, msb
//SPCR= 8'b01011101; neg_edge, msb
  
//SPCR= 8'b01110101; pos_edge, lsb 
//SPCR= 8'b01110101; pos_edge, lsb 
  
//SPCR= 8'b01011001; pos_edge, msb
//SPCR= 8'b01011001; pos_edge, msb
  
  
 

reg [7:0] SPCR = 8'b01110001;
reg [7:0] SPSR = 8'b00000000;
reg [7:0] SPDR = 8'b01010101;

parameter idle=2'b01;
parameter sckgenerate=2'b10;

integer counter1,count=0;
integer counter;
integer mosicounter=0,mosicounter1=0;
integer  counter_4_SPSR;  
reg pulsebit;
reg [1:0] sckvalue;
reg [1:0] state = idle;
reg sck_buffer=1;
reg mosifalling;
reg mosirising;
reg start = 1'b1;
reg [7:0] mosi_temp = 8'b01010101; 
reg [7:0] miso_temp = 8'b00000000; 
wire sck_negedge;
reg sck_reg0,sck_reg1;  
reg dir = 1'b1; 
reg ssel = 0;
reg [7:0] register=8'b00000000;  
integer  delay_counter = 0; 
//  assign SPDR = (dir== 1)?mosi_temp: miso_temp;  
//pulsebit

always @ (posedge clk)
begin  
  if (rst == 1)
    counter1 <= 0;
  else
    begin
     if (counter1 >= counter)
        begin
	     counter1 <= 0;
         pulsebit <= 1;
        end //inner if 
       else 
        begin
         counter1 <= counter1 + 1;
         pulsebit <= 0;
        end //inner else		
    
    end // outer else  	
end //always

//counter

always @ (posedge clk)
begin
  sckvalue <= SPCR[1:0];

   case (sckvalue)
    2'b00 : 
            counter <= 1;
    2'b01:
            counter <= 7;
    2'b10:  
            counter <= 31;
    2'b11:
            counter <= 63;  
  
   endcase
end //always

//sck

always @ (posedge clk)
begin
case (state)
 
 idle: 

    begin
      if (start)
         begin
            if (SPCR[3] == 1)
              begin   
                sck_buffer <= 1;
                state <= sckgenerate;

              end
	        else 
               begin  
                 sck_buffer <= 0; 
                 state <= sckgenerate;

               end

        end //if loop
    end // idle

 sckgenerate:
    begin
      if (pulsebit == 1)
        begin
         sck_buffer <= ~sck_buffer;
        end
    end
endcase
   
end // sckgenerate		   

assign sck = sck_buffer;
  

always @ (posedge clk)
begin
  case (ssel)
     0 :   begin
           ss <= 1;            
             
       if(delay_counter == 20)
    			ssel <= 1;
  		    else
    			delay_counter <= delay_counter + 1;
           end 
    1: 
           begin
             ss <= 0;
             
           end  
  endcase
    
end  
  
  
  
//mosi falling
always @(negedge sck_buffer)
begin
  if ((SPCR[5] == 1) && (SPCR[3] == SPCR[2])) //lsb
      
			begin
              $display("slave sel initial %d",ss);
    		  if ( dir  == 1'b1)
     		   begin
                 $display("slave sel %d",ss);
                 if (ss == 0)
                   begin
	   				if (mosicounter > 7)
             			begin
		      				mosicounter <= 0;
		      				start <= 1'b0;
							dir <= 1'b0;
                          SPCR[7] <= 1'b1;
	   				    end // mosi counter
	  				 else 
  	     				begin
                			mosifalling <= SPDR[0];
                          SPDR <= {miso,SPDR[7:1]};
		        			mosicounter <= mosicounter + 1;
                          SPCR[7] <= 1'b0;
	     				end //else
                   end
       			 end //dir	
              if (count == 7) 
                dir <= 1;
		end // if loop

	
    	else
        	 begin
         		 mosifalling <= 1'b0;  
               
         	 end    

     // end //else
end //always

always @(negedge sck_buffer)
begin
  if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
			begin
     		if (dir == 1'b1)
      		  begin
               if (ss == 0)
                 begin
			      if (mosicounter >= 7)
             			begin
		       				mosicounter <= 0;
               				start <= 1'b0;
               				dir <= 1'b0;
                          SPCR[7] <= 1'b1;
	     				end // mosi counter
	   				else 
  	     				begin
                			mosifalling <= SPDR[7];
							SPDR <= {SPDR[6:0],1'b0};
							mosicounter <= mosicounter + 1;
                          SPCR[7] <= 1'b0;
	     				end //else
                   end  
                  end //	dir	
		end // elseif loop	
end //always

  
  

//mosi rising
always @(posedge sck_buffer)
begin
  if ((SPCR[5] == 1) && (SPCR[3] != SPCR[2])) //lsb
	begin
      if (dir == 1)
        begin
          if (ss == 0)
            begin

          if (mosicounter >= 7)
             		begin
					mosicounter <= 0;
               		start <= 1'b0;
               		dir <= 1'b0;
                    SPCR[7] <= 1'b1;  
           			end // mosi counter
	   			else 
  	     			begin
		
	                mosirising <= SPDR[0];
    		       SPDR <= {1'b0,SPDR[7:1]};
				   mosicounter <= mosicounter + 1;
                      SPCR[7] <= 1'b0;   
	     		end //else
            end
        end //dir 		
	end // if loop

    else
         begin
          mosifalling <= 1'b0;  
               
         end    


end //always

  
always @(posedge sck_buffer)
begin
  
  if ((SPCR[5] == 0) && (SPCR[3] != SPCR[2])) //msb
	begin
      if (dir == 1)
        begin
        if (ss == 0)
          begin
          if (mosicounter >= 7)
            		 begin
					mosicounter <= 0;
					start <= 1'b0;
               		dir <= 1'b0;
                    SPCR[7] <= 1'b1;   
	     			end // mosi counter
	   			else 
  	    			 begin
		
    		            mosirising <= SPDR[7];
						SPDR <= {SPDR[6:0],1'b0};
						mosicounter <= mosicounter + 1;
                       SPCR[7] <= 1'b0;
	     			end //else	
          end //ss
        end // dir		
	end // elseif loop	
end //always
	
    
  assign   mosi = ((SPCR[5] == 1) &&(SPCR[3] == SPCR[2]))? mosifalling:mosirising;
//assign SPDR = (dir== 1)?mosi_temp: miso_temp;  

//  assign SPSR[7] = (activemosi== 1)?1:0;
  
  always @(*)
  begin
    if (dir ==1)
      SPSR[7] <= 1;
    else
      SPSR[7] <= 0;
 
  end


//MISO

//miso rising
always @(posedge sck_buffer)
begin
if(SPSR[7] == 0)
begin
  if ((SPCR[5] == 1) && (SPCR[3] == SPCR[2])) //lsb
	begin
	       
      miso_temp <= {miso,miso_temp[7:1]};
      
        count <= count + 1;
	end // if loop

    
    

end //while
end //always

always @(posedge sck_buffer)
begin
if(SPSR[7]==0)
begin

  if ((SPCR[5] == 0) && (SPCR[3] == SPCR[2])) //msb
	begin
	       // miso_temp <= {miso_temp[6:0],miso};
		    count <= count + 1;
	end // elseif loop	
end //while
end //always

  
//mosi falling
always @(negedge sck_buffer)
begin
if(SPSR[7]==0)
begin
  if ((SPCR[5] == 1) && (SPCR[3] != SPCR[2])) //lsb
	begin
	       
		//miso_temp <= {miso,miso_temp[7:1]};
      count <= count + 1;
	end // if loop

       

end //while
end //always
  
always @(negedge sck_buffer)
begin
if(SPSR[7]==0)
begin

  if ((SPCR[5] == 0) && (SPCR[3] != SPCR[2])) //msb
	begin
    //  miso_temp <= {miso_temp[6:0],miso};
      count <= count + 1;
		
	end // elseif loop	
end //while
end //always
	
    
endmodule