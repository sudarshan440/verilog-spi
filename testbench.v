// Code your testbench here
// or browse Examples
module tb;

reg clk,rst;

  top u2 (clk,rst);
  
initial 
begin
  $dumpfile("dump.vcd");
  $dumpvars(0,tb);

 clk = 0;
rst = 1;


end //initial

initial
begin
  #100 rst = 0;
 #6000;
$finish;
end 
  
always #10 clk = ~clk;

endmodule 